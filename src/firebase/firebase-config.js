import 'firebase/firestore';
import 'firebase/auth';
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';
import { GoogleAuthProvider } from 'firebase/auth';


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDJgIRTtYFqxq7LgoO0gQ7KLCeX4m8rJF8",
    authDomain: "react-app-cursos-udemy-e8880.firebaseapp.com",
    projectId: "react-app-cursos-udemy-e8880",
    storageBucket: "react-app-cursos-udemy-e8880.appspot.com",
    messagingSenderId: "631934814020",
    appId: "1:631934814020:web:4fbc6fc1b7f78d96917911"
};

// Initialize Firebase
 initializeApp(firebaseConfig);

const db = getFirestore();

const googleAuthProvider = new GoogleAuthProvider();

export{
    db,
    googleAuthProvider
}